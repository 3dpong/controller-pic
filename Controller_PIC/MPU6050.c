// Hoek berekenen (degrees per second integreren)
#include "MPU6050.h"
#include "PIC_Timer1.h"
#include "I2C.h"

// previous angles are calibrated on 0 degrees.
unsigned int prevAngle_X =0;
unsigned int prevAngle_Y =0;
unsigned int gyroX = 0;
unsigned int gyroY = 0;
unsigned int time1_XY = 0;
//int prevTime_X = 0;
//int prevTime_Y = 0;
void initMPU(){
    write_register(0x6B, 0); // Disable MPU6050 sleep mode
}

/*
  ---------
  In this method, both the x and the y values of the gyroscope are defined.
  Beware that the values are not extremely accurate, since they both use the
  same delta t(ime): The time is read from timer1 and saved. Next, both values
  use the same time value in their calculation. This is inconvenient since the
  reading of the gyroscope values take a lot of time due to the I2C protocol
  and it now has to read two times, making the timer value less reliable.
  Ideally, the timer and both gyroscope values would be read instantanious and 
  at the same time. But with limited recources this is not possible.
  ---------
*/
void readGyroXY(){
    // read timer1 to variable
    time1_XY = readTimer1();
    // Update sensor data and instantly reset and start timer1
    gyroX = (read_register(gyX) << 8) | read_register(gyXlsB);
    resetTimer1();
    gyroY = (read_register(gyY) << 8) | read_register(gyYlsB);
    // formule[ gyrox * timer1 + prevAngle_X = new angle ]
    gyroX = (gyroX * time1_XY) + prevAngle_X;
    gyroY = (gyroY * time1_XY) + prevAngle_Y;
    prevAngle_X = gyroX;
    prevAngle_Y = gyroY;
}