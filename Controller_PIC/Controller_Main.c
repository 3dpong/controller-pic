#include "I2C.h"
#include "PIC_init.h"
#include "PIC_Timer1.h"
#include "GPIO.h"
#include "MPU6050.h"
#include "Manchester.h"

#define OSCTUNE (* (unsigned int *) 0x98)
#define OSCCON (* (unsigned int *) 0x99)
unsigned int syncByte= 0xAA;

void wait(int times){
     int i;
     for( i = 0; i < 500; i++){
        asm {nop}
     }
}
void CPU_clock_init(){
     OSCCON = (OSCCON & 0xFF00) | 0b11110000;   //set clock to 8mHz with internal osc and 4xPLL
}
void firstRead(){
   resetTimer1();
   busrt_read_mpu6050(0x3B); // Refresh gyro & acc data
}

void main() {

     CPU_clock_init(); // Setup pic cpu clock speed
     i2c_init();
     initManchester();
     initTimer1();
     firstRead();


     while(1){
         busrt_read_mpu6050(0x3B); // Refresh gyro & acc data
         readTimer1();
         resetTimer1();
         //send bytes to radiometrix via manchester coding
         sendByte(syncByte);
         sendMPU_Bytes();
         sendTimer_Bytes();
     }
}