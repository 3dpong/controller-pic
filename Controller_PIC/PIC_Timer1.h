//PIC_Timer1.h

// TMR1H:TMR1L
   //A dedicated low-power 32.768 kHz oscillator circuit is
   //built-in between pins T1OSI (input) and T1OSO
   //(amplifier output).

   //The oscillator circuit is enabled by setting the
   //T1OSCEN bit of the T1CON register

#ifndef PIC_Timer1_h_
#define PIC_Timer1_h_

#include "GPIO.h"

//Holding Register for the Least Significant Byte of the 16-bit TMR1 Register
#define  TMR1L   (* (unsigned int *) 0x016)
//Holding Register for the Most Significant Byte of the 16-bit TMR1 Register
#define  TMR1H   (* (unsigned int *) 0x017)



//timer1 control reg
#define T1CON    (* (unsigned int *)  0x018)
//timer1 gate control reg
#define T1GCON   (* (unsigned int *)  0x019)

void sendTimer_Bytes();
void testTimer1();
void initTimer1();
void startTimer1();
void resetTimer1();
void stopTimer1();
unsigned int readTimer1();
#endif