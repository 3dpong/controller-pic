//Timer.c
#include "PIC_Timer1.h"
#include "GPIO.h"
#include "Manchester.h"
unsigned int timer1Value;
unsigned int TMR1_H_Buffer;
unsigned int TMR1_L_Buffer;
unsigned int counter1=0;


//test method
void testTimer1(){   //3,042982 MHz (mogelijk meer)
        if (timer1Value >= 50000){
           resetTimer1();
           counter1++;
           //61 bij 3,04MHz
        if (counter1 == 30){ // 0.5 seconde
           writePortC(2, 1);
           writePortC(2, 0);
           counter1 = 0;
        }
        }
}


         //T1CON register to control timer1  *page 185
void initTimer1(){
    // T1CON = 1001 1110
    // T1CON would then be: 32kHz osc, prescale 1:2, timer1 disabled.
    T1CON     = (T1CON & 0xFF00) | 0b01000000;
    // 01000000 is Fosc (8MHz)
    // 00011100 is bekende werkende waarde.
}



void startTimer1(){
    // TMR1ON bit 1 enables 0 disables/resets gate
    T1CON |= (1 <<  0);
}
void resetTimer1(){
    // stop timer1
    T1CON &= ~(1 << 0);
    TMR1H=0;
    TMR1L=0;
    // start timer1
    T1CON |= (1 << 0);
}
void stopTimer1(){
     T1CON &= ~(1 << 0);
}

void sendTimer_Bytes(){
     sendByte(TMR1_H_Buffer);
     sendByte(TMR1_L_Buffer);
}


unsigned int readTimer1(){
      //unsigned int timer1Value = 0;
      //start read procedure
      TMR1_H_Buffer = TMR1H;
      TMR1_L_Buffer = TMR1L;

      if (TMR1_H_Buffer - TMR1H != 0){
         //overflow
         TMR1_H_Buffer = TMR1H;
         TMR1_L_Buffer = TMR1L;
         }
      //int number = buf[0] | buf[1] << 8;
      // http://en.cppreference.com/w/c/language/operator_precedence the << operator has priority over the | operator.
       timer1Value = TMR1_L_Buffer | TMR1_H_Buffer << 8;
       return timer1Value;
    /*
     asm{
          MOVF TMR1H, W            ; //Read high byte
          MOVWF TMR1_H_Buffer      ; //Save this value
          MOVF TMR1L, W            ; //Read low byte
          MOVWF TMR1_L_Buffer      ; //Save this value
          MOVF TMR1H, W            ; //Re-Read high byte
          SUBWF TMR1_H_Buffer, W   ; //Sub 1st read with 2nd read
          BTFSC STATUS, Z          ; //Is result = 0
          GOTO label               ; //No overflow occurred
                                     //Else, an overflow has occurred
                                     //Reading the high and low bytes now will read a good value.
          MOVF TMR1H, W            ; //Read high byte
          MOVWF TMR1_H_Buffer      ; //Save this value
          MOVF TMR1L, W            ; //Read low byte
          MOVWF TMR1_L_Buffer      ; //Save this value
label:                               //Continue with your code
          }
          */
}