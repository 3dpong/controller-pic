//MPU6050.h
#ifndef MPU6050_h_
#define MPU6050_h_
#include "I2C.h"

void readGyroXY(); // returns new X angle
void initMPU();

#endif