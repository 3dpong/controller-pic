//MPU6050.h
#ifndef Manchester_h_
#define Manchester_h_
#include "GPIO.h"


void sendBit(int data_bit);
void initManchester();
void sendByte(int data_byte);

#endif