#include "Gpio.h"
#include <stdlib.h>

int Manchester_pin = 2; // pin that needs to be connected to the FM transmitter
int Data_pin = 1;       // only used to see raw data for testing
int j = 0;

void sendBit(int data_bit){
     if (data_bit == 1){                // send 1
        writePortC(Data_pin, 1);        // data pin = 1
        writePortC(Manchester_pin, 0);  // manch pin = 0
        writePortC(Manchester_pin, 1);  // manch pin = 1
     }
     if (data_bit == 0) {               // send 0
         writePortC(Data_pin, 0);       // data pin = 0
         writePortC(Manchester_pin, 1); // manch pin = 1
         writePortC(Manchester_pin, 0); // manch pin = 1
     }
}

void SendByte(int data_byte){
     int i, data_bit = 0;
     sendBit(0);                           // start bit
     for (i = 7; i >= 0; i--){
         data_bit = (data_byte >> i) & 1; // right shift
         sendBit(data_bit);               // end bit
      }
      sendBit(1);
}

void main() {
     int byte = 0xCA;
     setDirC(Manchester_pin, 0);
     setDirC(Data_pin, 0);
     while(1){
          writePortC(Data_pin, 0);           // all signals low
          writePortC(Manchester_pin, 0);
          SendByte(byte);                    // send byte
          writePortC(Data_pin, 0);           // all signals low
          writePortC(Manchester_pin, 0);
          for (j = 0; j < 50; j++){ NOP(); } // delay before next
     }
}