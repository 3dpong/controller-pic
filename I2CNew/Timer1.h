//Timer1.h

#ifndef Timer1_h_
#define Timer1_h_

#include "Gpio.h"

//Holding Register for the Least Significant Byte of the 16-bit TMR1 Register
#define  TMR1L   0x016)
//Holding Register for the Most Significant Byte of the 16-bit TMR1 Register
#define  TMR1H   0x017




#define T1CON    (* (unsigned int *)  0x018)
#define T1GCON   (* (unsigned int *)  0x019)

         // TMR1H:TMR1L
   //A dedicated low-power 32.768 kHz oscillator circuit is
   //built-in between pins T1OSI (input) and T1OSO
   //(amplifier output).
   
   //The oscillator circuit is enabled by setting the
   //T1OSCEN bit of the T1CON register
unsigned int readTimer1();





#endif