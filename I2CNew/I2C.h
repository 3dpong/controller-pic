#include "Gpio.h"
#define HIGH 1
#define LOW 0
#define OUTPUT 0
#define INPUT 1

#define acX 0x3B
#define acY 0x3D
#define acZ 0x3F

#define gyX 0x43
#define gyY 0x45
#define gyZ 0x47

#define DEVICE_MEMORY_ADDRESS  0x40
#define DONT_CARE 0x0
#define SLAVE_ADDRESS 0xD0 //check 0X68 << 1

#define READ 0x1
#define WRITE 0x0

                  int write_register(int address, int data1);
int i2c_read_byte();
void i2c_write_byte(int data1);
void i2c_write_nibble(int data1);
void i2c_write_bit(int bitValue);
void i2c_start_condition();

void i2c_stop_condition();
void i2c_write_ack();
void i2c_write_nack();
void digitalWrite(int, int);
int digitalRead(int);
void pinMode(int data1, int value1);

int NACK();
int read_register(int address);