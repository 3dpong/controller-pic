#include "I2C.h"

int dataByte1 = 0xff;
int inputData;
int dum; //dummy bit to read NACK

int SCL_pin = 0; // c0
int SDA_pin = 1; // c1

int i;
int data1;

int i2c_read_byte() {
        int data1 = 0;
        int i = 0;
        pinMode(SDA_pin, INPUT);

        for ( i = 0; i < 8; i++) {
         int currentBit = 0;
                digitalWrite(SCL_pin, HIGH);
                currentBit = digitalRead(SDA_pin);
                data1 = (data1 << 1);
                data1 = (data1 | currentBit);
                digitalWrite(SCL_pin, LOW);
  }
  pinMode(SDA_pin, OUTPUT);
  return data1;
}
void i2c_write_byte(int data1) {
        int i = 0;
        for ( i = 7; i >= 0; i--) {
                i2c_write_bit((data1 >> i) & 1 );
        }
}
void i2c_write_nibble(int data1) {
int i = 0;
  for ( i = 3; i >= 0; i--) {
    i2c_write_bit((data1 >> i) & 1 );
  }
}
void i2c_write_bit(int bitValue) {
  pinMode(SCL_pin, OUTPUT); // Extra check
  digitalWrite(SDA_pin, bitValue);
  digitalWrite(SCL_pin, HIGH);
  digitalWrite(SCL_pin, LOW);
}
void i2c_start_condition() {
int i;
  pinMode(SDA_pin, OUTPUT);
  pinMode(SCL_pin, OUTPUT);
  /*
  pinMode(2, OUTPUT);
  while (1){
    digitalWrite(SDA_pin, HIGH);
    digitalWrite(SCL_pin, HIGH);
        digitalWrite(2, HIGH);
            digitalWrite(2, LOW);
    //digitalWrite(SDA_pin, LOW);
    //digitalWrite(SCL_pin, LOW);
  }

  digitalWrite(SCL_pin, HIGH);
  digitalWrite(SDA_pin, LOW);
  digitalWrite(SCL_pin, LOW);
  */
  pinMode(SDA_pin, OUTPUT);
  digitalWrite(SDA_pin, HIGH);
  digitalWrite(SCL_pin, HIGH);
  digitalWrite(SDA_pin, LOW);
  digitalWrite(SCL_pin, LOW);
}
void i2c_stop_condition() {
  pinMode(SDA_pin, OUTPUT);
  digitalWrite(SDA_pin, LOW);
  digitalWrite(SCL_pin, HIGH);
  digitalWrite(SDA_pin, HIGH);
}
void i2c_write_ack() {
  pinMode(SCL_pin, OUTPUT); // Extra check
  digitalWrite(SDA_pin, 0);
  digitalWrite(SCL_pin, HIGH);
  digitalWrite(SCL_pin, LOW);
}
void i2c_write_nack() {
  pinMode(SCL_pin, OUTPUT); // Extra check
  digitalWrite(SDA_pin, 1);
  digitalWrite(SCL_pin, HIGH);
  digitalWrite(SCL_pin, LOW);
}
int NACK() {
  pinMode(SDA_pin, INPUT); // DEZE REGELE LOOPT VAST// Release SDA //input pullup
  digitalWrite(SCL_pin, HIGH);
  //hier komt hij al niet
  if (digitalRead(SDA_pin) == HIGH) {
    digitalWrite(SCL_pin, LOW);
    pinMode(SDA_pin, OUTPUT);
    return 1;
  } else {
    digitalWrite(SCL_pin, LOW);
    pinMode(SDA_pin, OUTPUT);
    return 0;
  }
}
int read_register(int address) {
  i2c_start_condition(); // Send a start condition to the slave
  i2c_write_byte(SLAVE_ADDRESS);
  dum = NACK();
  if(dum == 1){return 0;}  //
  i2c_write_byte(address); //given hexadecimal register address
  dum = NACK();
  if(dum == 1){return 0;} //
  i2c_start_condition(); // Repeated start
  i2c_write_byte(SLAVE_ADDRESS | READ);
  dum = NACK();
  if(dum == 1){return 0;}///
  inputData = i2c_read_byte();
  i2c_write_nack();
  i2c_stop_condition();
  return inputData;
}
int write_register(int address, int data1) {
  i2c_start_condition(); // Send a start condition to the slave
  i2c_write_byte(SLAVE_ADDRESS);
  dum = NACK();
  if(dum == 1){return 0;}  //
  i2c_write_byte(address); //given hexadecimal register address
  dum = NACK();
  if(dum == 1){return 0;} //
  i2c_write_byte(data1);
  dum = NACK();
  if(dum == 1){return 0;}///
  return 0;
}
void digitalWrite(int pinNr, int mode){
     writePortC(pinNr, mode);
}

int digitalRead(int pinNr){
    int returnVal;
    returnVal = readPortC(pinNr);
    return returnVal;
}

void pinMode(int pinNr, int mode){
     setDirC(pinNr, mode);
}