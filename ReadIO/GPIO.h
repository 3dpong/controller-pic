//GPIO.h

#ifndef GPIO_H_
#define GPIO_H_

//    set direction in registers (1 for read, 0 for write. Default is 1.)
#define      TRISC  (* (unsigned int *)  0x08E) //00
#define      TRISA  (* (unsigned int *)  0x08C)   //00

//    read/write value of/to pins
#define      PORTA  (* (unsigned int *) 0x00C)     //00
#define      PORTC  (* (unsigned int *) 0x0E)//e
#define      LATC  (* (unsigned int *) 0x10E)//e
#define      ANSELA (* (unsigned int *) 0x18C)
#define      ANSELC (* (unsigned int *) 0x18E)
#define      WPUC (* (unsigned int *) 0x20E)
#define      OPTION_REG (* (unsigned int *) 0x95)

//    methods
void writePortA(int pinNr, int value);
void writePortC(int pinNr, int value);
void setDirA(int pinNr, int value);
void setDirC(unsigned int pinNr, unsigned int value);
int readPortA(int pinNr);
int readPortC(int pinNr);

#endif /* GPIO_H_ */