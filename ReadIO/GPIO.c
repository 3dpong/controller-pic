//GPIO.c

#include "GPIO.h"

//!! WARNING !!
//Do not write to TRIS or PORT A.6 and A.7
//These bits are not available or reserved.

void writePortA(int pinNr, int value){ //0x00C00  Port A goes from A.0 to A.5
   if (value==0){
        PORTA |= (0 << pinNr);
        } else {
        PORTA |= (1 << pinNr);
        }
}

void writePortC(int pinNr, int value){ //0x00E00  Port C goes from C.0 to C.7
      if (value == 0){
       //// PORTC &=  ~(1 << pinNr);
       LATC &= ~(1<<pinNr);
      } else {
        LATC |= (1 << pinNr);
        //PORTC |= 0b11;
      }
}
       //TRISC &= 0xFF00  portc to input
void setDirA(int pinNr, int value){  //sets one pin on TRISA to output (value 0) or input (value 1). Default is 1.
        if (value==0){
           TRISA &= ~(1 << pinNr);
        } else {
          //TRISA |= (1 << pinNr);
         // ANSELA &= ~(1 << pinNr);
         //ANSELA |= 0X0;
        }
}

void setDirC(unsigned int pinNr, unsigned int value){  //sets one pin on TRISC to output (value 0) or input (value 1). Default is 1.
     if (value==0){
        TRISC &= ~(1 << pinNr);    //output
        ANSELC |= (1 << pinNr);

     } else {

      TRISC |= (1 << pinNr);         //input
      ANSELC &= ~(1 << 1); //HIER GAAT HET FOUT
    //  WPUC &= ~(1<<pinNr); // Disables pullup    // auto when output
     OPTION_REG &= ~(1 << 7); // enable pullup modes
        WPUC |= (1<<pinNr); // Enables pullup     for individual pin

     }
}

int readPortA(int pinNr){
    return ((PORTA >> (pinNr)) & 1);
}

int readPortC(int pinNr){
    return ((PORTC >> (pinNr)) & 1);
}